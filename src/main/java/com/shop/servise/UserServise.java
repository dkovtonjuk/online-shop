package com.shop.servise;

import com.shop.dao.UserDao;

public class UserServise {

    private UserDao userDao = new UserDao();

    public boolean isUserWithUsernameExists(String userName) {
        return userDao.getUserByUsername(userName) != null;
    }

    public void createNewUser(String username, String password) {
        userDao.createUserBy(username, password);
    }
}
