package com.shop.model;

public class Product {
    private String productName;
    private int price;
    private int quantity;
    private int barcode;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getBarcode() {
        return barcode;
    }

    public void setBarcode(int barcode) {
        this.barcode = barcode;
    }

    public Product(String productName, int price, int quantity, int barcode) {
        this.productName = productName;
        this.price = price;
        this.quantity = quantity;
        this.barcode = barcode;
    }
}
