package com.shop.controller;

import com.shop.servise.UserServise;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/register")

public class RegisterServlet extends HttpServlet {

    private UserServise userService = new UserServise();
    private  Logger logger = LoggerFactory.getLogger("RegisterServletLogger");

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        try {
            request.getRequestDispatcher("/register.jsp").forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String username = request.getParameter("username");
        String password = request.getParameter("password");
        logger.debug("Got register request with username " + username);
        if (userService.isUserWithUsernameExists(username)) {
            logger.error("Username with username " + username + " exists!");
            request.setAttribute("errorMessage", "User with such username exists!");
            request.getRequestDispatcher("/register.jsp").forward(request, response);
        } else {
            logger.debug("will register username " + username);
            userService.createNewUser(username, password);
            request.getRequestDispatcher("/login.jsp").forward(request, response);

        }
    }
}
