package com.shop.dao;

import com.shop.dao.connection.DatabaseConnection;
import com.shop.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.transform.Result;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

//method

public class UserDao {

    private Connection connection = DatabaseConnection.getDBConnection();

    private Logger logger = LoggerFactory.getLogger(this.getClass());
   public User getUserByUsername (String username) {
       PreparedStatement stmt = null;
       User user = null;

       String sql = "SELECT * FROM USERS WHERE USERNAME = ?";
       try {
           stmt = connection.prepareStatement(sql);

           stmt.setString(1, username);

           ResultSet rs = stmt.executeQuery();

           while (rs.next()) {
               String usrName = rs.getString("USERNAME");
               String pswd = rs.getString("PASSWORD");
               String role = rs.getString("ROLE");

                if (usrName != null && pswd != null && role != null) {
                    user = new User(usrName, pswd, role);
                } else return null;
           }
       } catch (SQLException e) {
           e.printStackTrace();
           logger.error("Error" + e);
       } finally {
           try {
               stmt.close();
           } catch (SQLException e) {
               e.printStackTrace();
               logger.error("Error" + e);
           }
       }
       return user;
   }

   public void createUserBy (String username, String password) {
       PreparedStatement stmt = null;


       String sql = "INSERT INTO USERS (USERNAME, PASSWORD) VALUES (?, ?)";
       try {
           stmt = connection.prepareStatement(sql);

           stmt.setString(1, username);
           stmt.setString(2, password);

           stmt.executeUpdate();
       } catch (SQLException e) {
           e.printStackTrace();
           logger.error("Error" + e);
       } finally {
           try {
               stmt.close();
           } catch (SQLException e) {
               e.printStackTrace();
               logger.error("Error" + e);
           }
       }


   }
}
