package com.shop.controller;

import com.shop.dao.ProductDao;
import com.shop.dao.UserDao;
import com.shop.model.Product;
import com.shop.servise.ProductServise;
import com.shop.servise.UserServise;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(urlPatterns = "/admin/products/add")

public class AdminProductServlet extends HttpServlet {

    private ProductServise productServise = new ProductServise();
    private Logger logger = LoggerFactory.getLogger("AdminProductServletLogger");

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        try {
            request.getRequestDispatcher("/add.jsp").forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String productName = request.getParameter("productName");

        int price = Integer.parseInt(request.getParameter("price"));

        int quantity = Integer.parseInt(request.getParameter("quantity"));

        int barcode = Integer.parseInt(request.getParameter("barcode"));

        Product product = new Product(productName, price, quantity, barcode);

        productServise.saveNewProduct(product);

    }
}

