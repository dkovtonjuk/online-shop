package com.shop.servise;

import com.shop.dao.ProductDao;
import com.shop.model.Product;

public class ProductServise {

    private ProductDao productDao = new ProductDao();

    public void saveNewProduct(Product product) {
        productDao.save(product);
    }
}