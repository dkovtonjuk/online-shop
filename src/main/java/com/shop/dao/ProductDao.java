package com.shop.dao;

import com.shop.dao.connection.DatabaseConnection;
import com.shop.model.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductDao {

    private Connection connection = DatabaseConnection.getDBConnection();

    private Logger logger = LoggerFactory.getLogger(getClass());


    public void save(Product product) {
        PreparedStatement stmt = null;


        String sql = "INSERT INTO PRODUCTS (NAME, PRICE, QUANTITY, BARCODE) VALUES (?, ?, ?, ?)";
        try {
            stmt = connection.prepareStatement(sql);

            stmt.setString(1, product.getProductName());
            stmt.setInt(2, product.getPrice());
            stmt.setInt(3, product.getQuantity());
            stmt.setInt(4, product.getBarcode());

            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            logger.error("Error" + e);
        } finally {
            try {
                    stmt.close();

            } catch (SQLException e) {
                e.printStackTrace();
                logger.error("Error" + e);
            }
        }

    }
}


