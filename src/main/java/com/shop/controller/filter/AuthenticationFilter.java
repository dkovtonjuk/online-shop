package com.shop.controller.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

//@WebFilter("/*")
//public class AuthenticationFilter implements Filter {
//
//    @Override
//    public void init(FilterConfig fConfig) {
//
//    }
//
//    @Override
//    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
//        HttpServletRequest req = (HttpServletRequest) request;
//        HttpServletResponse res = (HttpServletResponse) response;
//
//        HttpSession session = req.getSession(false);
//
//        String loginURI = req.getRequestURI() + "/login";
//        String registerURI = req.getContextPath() + "/register";
//
//        boolean loggedIn = session != null && session.getAttribute("user" ) != null;
//        boolean isLoginRequest = req.getRequestURI().equals(loginURI);
//        boolean isRegisterRequest = req.getRequestURI().equals(registerURI);
//
//        if (loggedIn || isLoginRequest || isRegisterRequest) {
//            chain.doFilter(request, response);
//        } else {
//            res.sendRedirect(loginURI);
//        }
//
//    }
//
//    @Override
//    public void destroy() {
//
//    }
//}
